package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.LinkedList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        try {
            LinkedList<Double> digits = new LinkedList<>();
            LinkedList<Character> oper = new LinkedList<Character>();
            DecimalFormat df = new DecimalFormat("##.####");
            DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
            dfs.setDecimalSeparator('.');
            df.setDecimalFormatSymbols(dfs);


            for (int i = 0; i < statement.length(); i++) {
                char c = statement.charAt(i);
                if (c == ' ')
                    continue;
                if (c == '(')
                    oper.add('(');
                else if (c == ')') {
                    while (oper.getLast() != '(')
                        operations(digits, oper.removeLast());
                    oper.removeLast();
                } else if (c == '+' || c == '-' || c == '*' || c == '/') {
                    while (!oper.isEmpty() && order(oper.getLast()) >= order(c))
                        operations(digits, oper.removeLast());
                    oper.add(c);
                } else {
                    String operand = "";
                    while (i < statement.length() && Character.isDigit(statement.charAt(i)) | statement.charAt(i) == '.') {
                        operand += statement.charAt(i++);
                    }
                    i--;
                    digits.add(Double.parseDouble(operand));
                }
            }

            while (!oper.isEmpty())
                operations(digits, oper.removeLast());


            return df.format(digits.get(0));


        } catch (Exception ex) {
            return null;
        }

    }


    static void operations(LinkedList<Double> digits, char operation) {
        double second = digits.removeLast();
        double first = digits.removeLast();
        switch (operation) {
            case '+':
                digits.add(first + second);
                break;
            case '-':
                digits.add(first - second);
                break;
            case '*':
                digits.add(first * second);
                break;
            case '/':
                if (second == 0) digits.add(null);
                else digits.add(first / second);
                break;

        }
    }
    static int order (char operation) {
        switch (operation) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }
}